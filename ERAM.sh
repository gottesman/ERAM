#!/bin/bash

############################################################################
#    Kajii Narumi kajiikana69@gmail.com
#    Gabriel Gottesman gottesman007@gmail.com
#    Copyright (C) 2016 ERAM
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

#contraseña de ingreso al script
PASWD="ingresa la contraseña para el scrip"
#Nombre del Negocio o Escuela
NAME="agrega el nombre de la empresa o escuela"
#Puerto ssh
PORT="elige el puerto ssh"
#Explorador de archivos
ARCH="ingresa el gestor de archivos de tu distribucion"
#establesca informacion de los equipos
MAC=(0 "mac1" "mac2" "mac3")
IP=(0 "ip1" "ip2" "ip3")
USA=(0 "admin1" "admin2" "admin3")
PSA=(0 "contraseña admin1" "contraseña admin2" "contraseña admin3")
USU=(0 "usuario1" "usuario2" "usuario3")
PSU=(0 "contraseña usuario1" "contraseña usuario2" "contraseña usuario3")
#__________________________________________________
# si deceas quitar la contraseña agrega al inicio "#"
ACC1="Introdusca su contraseña:"
ACC=$(zenity --title="Bienvenido" --width=200 --height=100 --entry --hide-text --text "$ACC1")

if [ $ACC = ${PASWD} ]
 then zenity --info --width=300 --height=200 --text="Que gusto verte de vuelta (^_^)"
 else zenity --error --width=400 --height=200 --text="la contraseña $ACC es incorrecta"
exit
fi
#__________________________________________________
#accion a continuar

opcion=`/usr/bin/zenity --title="$NAME" --width=350 --height=400 \
                         --text="Selecciona una Acción" \
                         --list --column="Seleccionar" --column="Acción" \
                         --checklist FALSE "Encender" FALSE "Apagar" FALSE "Reiniciar" FALSE "Enviar Archivos Usuarios" FALSE "Enviar Archivos Admin" FALSE "Instalar Aplicaciones" FALSE "Actualizar" FALSE "Borrar Archivos" FALSE "Bloquear Equipos" FALSE "Desbloquear Equipos" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
        if [ "$opcion" = "Encender" ];
        then 
# INICIO DEL SCRIPT Encender Equipos
opcion=`/usr/bin/zenity --title="Encendiendo" --width=350 --height=500 \
                         --text="Selecciona un Equipo" \
                         --list --column="Seleccionar" --column="Equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
wakeonlan ${MAC[opcion]}
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
#FIN DEL SCRIPT Encender Equipos
        elif [ "$opcion" = "Apagar" ]
                     then
# INICIO DEL SCRIPT Apagar equipos
opcion=`/usr/bin/zenity --title="Apagandando" --width=350 --height=500 \
                         --text="Selecciona un Equipo" \
                         --list --column="Seleccionar" --column="equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
CM="sudo -S shutdown -h now"
expect -c "
set timeout -1
spawn ssh -o StrictHostKeyChecking=no -p ${PORT} ${USA[opcion]}@${IP[opcion]} echo ${PSA[opcion]} | $CM
match_max 100000
expect \"*?assword:*\"
send \"${PSA[opcion]}\r\"
send \"\r\"
expect eof
"
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
#FIN DEL SCRIPT Apagar equipos
        elif [ "$opcion" = "Reiniciar" ]
                     then
# INICIO DEL SCRIPT Reiniciar Equipos
opcion=`/usr/bin/zenity --title="Reinicio" --width=350 --height=500 \
                         --text="Selecciona un Equipo" \
                         --list --column="Seleccionar" --column="equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
CM="sudo -S shutdown -r now"
expect -c "
set timeout -1
spawn ssh -o StrictHostKeyChecking=no -p ${PORT} ${USA[opcion]}@${IP[opcion]} echo ${PSA[opcion]} | $CM
match_max 100000
expect \"*?assword:*\"
send \"${PSA[opcion]}\r\"
send \"\r\"
expect eof
"
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit

#FIN DEL SCRIPT Reiniciar Equipos
        elif [ "$opcion" = "Enviar Archivos Usuarios" ]
                     then
# INICIO DEL SCRIPT Enviar Archivos Usuarios

opcion=`/usr/bin/zenity --title="Envio de Archivos" --width=350 --height=500 \
                         --text="Selecciona el Equipo a Enviar" \
                         --list --column="Seleccionar" --column="Equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
${ARCH} sftp://${USU[opcion]}@${IP[opcion]}:${PORT}/home/${USU[opcion]}/Escritorio
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
#FIN DEL SCRIPT Enviar Archivos Usuarios
        elif [ "$opcion" = "Enviar Archivos Admin" ]
                     then
# INICIO DEL SCRIPT Enviar Archivos Admin

opcion=`/usr/bin/zenity --title="Envio de Archivos" --width=350 --height=500 \
                         --text="Selecciona el Equipo a Enviar" \
                         --list --column="Seleccionar" --column="Equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
${ARCH} sftp://${USA[opcion]}@${IP[opcion]}:${PORT}/home/${USA[opcion]}/Escritorio
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
#FIN DEL SCRIPT Enviar Archivos Admin
        elif [ "$opcion" = "Instalar Aplicaciones" ]
                     then
# INICIO DEL SCRIPT instalar
opcion=`/usr/bin/zenity --title="Encendiendo" --width=350 --height=500 \
                         --text="Selecciona un Equipo" \
                         --list --column="Seleccionar" --column="Equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
zenity --question \
--text="¿Tiene Repositorio?"
if [ $? -eq 0 ]
then
TX1="Introdusca unicamente el repositorio

...........Ejemplo:

 ppa:atareao/telegram"

TX2="Introdusca unicamente la apliacion

...........Ejemplo:

 telegram"
REPO=$(zenity --title="Agregando repositorio" --width=200 --height=100 --entry --text "$TX1")
APP=$(zenity --title="Agregando aplicaciones" --width=200 --height=100 --entry --text "$TX2")
CM="sudo -S add-apt-repository ${REPO} -y && sudo -S apt-get update && sudo -S apt-get install ${APP} -y && sudo -S apt-get update "
xterm -e expect -c "
set timeout -1
spawn ssh -o StrictHostKeyChecking=no -p ${PORT} ${USA[opcion]}@${IP[opcion]} echo ${PSA[opcion]} | $CM
match_max 100000
expect \"*?assword:*\"
send \"${PSA[opcion]}\r\"
send \"\r\"
expect eof
"
else
TX2="Introdusca unicamente la apliacion

...........Ejemplo:

 telegram"
APP=$(zenity --title="Agregando aplicaciones" --width=200 --height=100 --entry --text "$TX2")

CM="sudo -S apt-get update && sudo -S apt-get install ${APP} -y && sudo -S apt-get update "
xterm -e expect -c "
set timeout -1
spawn ssh -o StrictHostKeyChecking=no -p ${PORT} ${USA[opcion]}@${IP[opcion]} echo ${PSA[opcion]} | $CM
match_max 100000
expect \"*?assword:*\"
send \"${PSA[opcion]}\r\"
send \"\r\"
expect eof
"
exit
fi
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
# FIN DEL SCRIPT Instalar
        elif [ "$opcion" = "Actualizar" ]
                     then
# INICIO DEL SCRIPT Actualizar Equipos

opcion=`/usr/bin/zenity --title="Actualizando" --width=350 --height=500 \
                         --text="Selecciona el Equipo" \
                         --list --column="Seleccionar" --column="Equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
CM="sudo -S apt-get update && sudo -S apt-get dist-upgrade -y"
xterm -e expect -c "
set timeout -1
spawn ssh -o StrictHostKeyChecking=no -p ${PORT} ${USA[opcion]}@${IP[opcion]} echo ${PSA[opcion]} | $CM
match_max 100000
expect \"*?assword:*\"
send \"${PSA[opcion]}\r\"
send \"\r\"
expect eof
"
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
#FIN DEL SCRIPT Actualizar Equipos
        elif [ "$opcion" = "Borrar Archivos" ]
                     then
# INICIO DEL SCRIPT Borrar Archivos de Equipos

opcion=`/usr/bin/zenity --title="Limpiando" --width=350 --height=500 \
                         --text="Selecciona un Equipo" \
                         --list --column="Seleccionar" --column="equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
		CM="rm -rf ~/Descargas/* && rm -rf ~/Documentos/* && rm -rf ~/Escritorio/* && rm -rf ~/Imágenes/* && rm -rf ~/Música/* && rm -rf ~/Vídeos/* && rm -rf ~/.local/share/Trash/* "
        for opcion in $opcion
        do
expect -c "
set timeout -1
spawn ssh -o StrictHostKeyChecking=no -p ${PORT} ${USU[opcion]}@${IP[opcion]} echo ${PSU[opcion]} | $CM
match_max 100000
expect \"*?assword:*\"
send \"${PSU[opcion]}\r\"
send \"\r\"
expect eof
"
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit

#FIN DEL SCRIPT Borrar Archivos de Equipos
        elif [ "$opcion" = "Bloquear Equipos" ]
                     then
# INICIO DEL SCRIPT Bloquear
opcion=`/usr/bin/zenity --title="bloqueando" --width=350 --height=500 \
                         --text="Selecciona un Equipo" \
                         --list --column="Seleccionar" --column="Equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
echo stop ${USU[opcion]} >> historial
date +%D_%H:%M:%S_%s >> historial
date +%s >  ~/Escritorio/tem/tem2${USU[opcion]}
TEM1=$(cat ~/Escritorio/tem/tem1${USU[opcion]})
TEM2=$(cat ~/Escritorio/tem/tem2${USU[opcion]})
TIM3=$(echo $TEM2/60 - $TEM1/60 | bc)
COS=$(echo $TIM3 \* 0.17 | bc)

zenity --info \
--text="el tiempo fue ${TIM3} minutos
....$ ${COS} pesos"
  EOH

CM="sh lock.sh"
expect -c "
set timeout -1
spawn ssh -X -o StrictHostKeyChecking=no -p ${PORT} ${USU[opcion]}@${IP[opcion]} echo ${PSU[opcion]} | $CM
match_max 100000
expect \"*?assword:*\"
send \"${PSU[opcion]}\r\"
send \"\r\"
expect eof
"
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
# FIN DEL SCRIPT bloquear
        elif [ "$opcion" = "Desbloquear Equipos" ]
                     then
# INICIO DEL SCRIPT desbloquear
opcion=`/usr/bin/zenity --title="Liberando" --width=350 --height=500 \
                         --text="Selecciona un Equipo" \
                         --list --column="Seleccionar" --column="Equipo" \
                         --checklist FALSE "1" FALSE "2" FALSE "3" FALSE "4" FALSE "5" FALSE "6" FALSE "7" FALSE "8" FALSE "9" FALSE "10" `
if [ $? -eq 0 ]
then
        IFS="|"
        for opcion in $opcion
        do
echo start ${USU[opcion]} >> historial
date +%D_%H:%M:%S_%s >> historial
date +%s > ~/Escritorio/tem/tem1${USU[opcion]}

CM="sh unlock.sh"
expect -c "
spawn ssh -X -o StrictHostKeyChecking=no -p ${PORT} ${USU[opcion]}@${IP[opcion]} echo ${PSU[opcion]} | $CM
match_max 100000
expect \"*?assword:*\"
send \"${PSU[opcion]}\r\"
send \"\r\"
expect eof
"
        done
./ERAM.sh
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
# FIN DEL SCRIPT desbloquear
        fi
        done
#salir<
zenity --question \
--text="¿Desea salir?"
if [ $? -eq 1 ]
then
./ERAM.sh
else
zenity --info \
--text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
exit
fi
#salir >
else
/usr/bin/zenity --info --text="<b>Terminado!</b> Para mayor informcaion ,  \nvisita <b>http://elcomodelcomo.blogspot.mx/</b>"
fi
exit
